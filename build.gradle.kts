import java.util.Properties

plugins {
    kotlin("jvm") version "1.4.32"
    id("fabric-loom") version "0.8-SNAPSHOT"
    id("maven-publish")
}

java {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
}

// load props from parent project
val parentProps = rootDir.resolve("gradle.properties").bufferedReader().use {
    Properties().apply {
        load(it)
    }
}

val modId: String by parentProps
val modVersion: String by parentProps
val group: String by parentProps
val minecraftVersion: String by parentProps

project.group = group
version = modVersion

repositories {
    maven(url = "https://maven.fabricmc.net/") {
        name = "Fabric"
    }
    maven(url = "https://kotlin.bintray.com/kotlinx") {
        name = "Kotlinx"
    }
    mavenLocal()
    mavenCentral()
}

minecraft {
}

dependencies {
    minecraft(group = "com.mojang", name = "minecraft", version = minecraftVersion)
    mappings(group = "net.fabricmc", name = "yarn", version = minecraftVersion + "+build.9", classifier = "v2")

    modImplementation("net.fabricmc:fabric-loader:0.11.5")
    modImplementation("net.fabricmc.fabric-api:fabric-api:0.35.0+1.16")
    modImplementation("net.fabricmc:fabric-language-kotlin:1.6.1+kotlin.1.5.10")
}

val fabricApiVersion = ""
val kotlinVersion = ""

tasks.getByName<ProcessResources>("processResources") {
    filesMatching("fabric.mod.json") {
        expand(
            mutableMapOf(
                "modid" to modId,
                "version" to modVersion,
                "kotlinVersion" to kotlinVersion,
                "fabricApiVersion" to fabricApiVersion
            )
        )
    }
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            groupId = group
            artifactId = modId
            version = version

            from(components["java"])
        }
    }
}
